Alberto Galan CV
-----------------



<img src="https://i.imgur.com/AFrDopd.png" width="140" height="162" /> <img src="https://i.imgur.com/FVQcQHr.png" width="140" height="162" />

https://github.com/albertogalan

Phone : +8618721846147

E- mail : <info@albertogalan.com>

## Last News

Currently preparing for CISSP - Cibersecurity Certification

## **Languages** 
Spanish, English, Catalan, Chinese

## **Programming Languages/Frameworks/Technologies** 

Bash, Python, Nodejs, Javascript, Html, Css, R

- Microservices Linux containers / LXD 
- Metasploit framework
- OWASP framework for manual testing webs app
- Jupyter/Anaconda for Datamining
- Github collaboration tool 
- Ansible automation configuration

OS( system admin ) : Debian Family, CentOs Family

## **Collaboration**

1<sup>st</sup> member in Coderbunker.com . Coderbunker is the first software developer community in Shanghai

## Valuable Assisted Conferences

[Radare Reverse engineering](https://rada.re) - Barcelona - October 2018 

[Hardwear Security conference](https://hardwear.io/)  - Netherlands - October 2018 

[Defcon China 1.0](https://www.defcon.org/html/dc-china-1/dc-cn-1-cfp.html) - Beijing May/June 2019

## **Digital Project experiences**

**Devops/Maintenance -- System Linux Admin and Devops – Ansible - VMware and AWS** – June 2018 - Now SH

- Monitor system (Nagios) setup and maintenance through SNMP checks 

- Use Ansible to automate the process creating a standard monitor system


**Automate Trading Digital Coin Investment** – Mar - Jun 2018 - Shanghai

- Persuading Digital Coin investors to get profit from automation

- Negotiating seed investment worth 500,000 RMB

**Developing -- Translation Web Appi** – Node.js, JS and Python – Jan – Mar 2018 - Shanghai

- Fullstack Developing

- Multiple functionalities to learn and understand Chinese documents as OCR and automatic translation

**Devops infraestructure -- Devops Mining Rig construction** – Ansible - Dec 2017 - Shanghai

-  Infraestructure Delivery. a 6 AMD GPUs through Ansible automation tools – calculated expecting ROI 200%

**Data minining -- Market Research Import/Export** – JScript - Oct 2017 - Shanghai

-   Scraping information public tender database – reduce the amount of time 1/1000 to get the key companies are working with metal products

**Devops infraestructure --  for DLG Digital Luxury Agency** – Ansible – Jul -Aug 2017 - Shanghai

-   Automated Deploying Digital Infrastructure for **digitalsociety.com –** avoid errors and reduce amount of time to maintain servers 1/50

**Devops for eiceducational.com** - Ansible - Dec 2016 - Shanghai

-   Deploying maintaining Server Infrastructure – avoid errors and reduce amount of time to maintain server 1/50

**Data analysis** – JScript – 2017 – Shanghai

-   Financial data, Scraping information around 1 million rows – analyze which are the customers, market research analysis

-   Import Export Data, Scraping information – market research analysis

**Data analysis** – R – 2013/2014 – Barcelona

-   Factorial Analysis with Jupyter– Customer Market Research

## **Business Developer and Marketing experience**

**Business Developer** - [www.grandtop.cn](http://www.grandtop.cn/) February 2017 - March 2019, Shanghai

-   Preparing to create a Spanish Company with Chinese Partner -

-   Europe and USA Lead research – found around 50 companies/laboratories

-   Business mediator between Spanish Customer and Chinese supplier

-   Found 1 project of around 2 mEuros in Madrid, the biggest Electric laboratory in Spain

**Business Developer** – [www.tornae.com](http://www.tornae.com/) - 2018 – now, Shanghai

-   Creating my own business.

-   International Business Development as a Purchase agency between China suppliers and Spanish buyers – Found one customer

**Marketing Manager** - [www.prittypigments.com](http://www.prittypigments.com/) - Sep 2015 – Jan 2017, Shanghai

-   Persuading to invest in a Financial Database to retrieve key data

-   Providing key customers around the world around 1000 from a Global Financial Data Provider – Give key information in a small amount of time.

-   Website building and maintaining, SEO analysis

**[Marketing Manager](http://www.linkedin.com/vsearch/p?title=Marketing+Manager&trk=prof-exp-title) - [www.etsystems.com](http://www.etsystems.com/)** – Jan 2010 – Jan 2015, Barcelona


-   Through Customer Market research and Persuade Techniques I created following projects:

    -   Textile Retail: www.zara.com : 2-3 mEuros

    -   Textile Retail: [www.desigual.com](http://www.desigual.com/) : 2 mEuros

    -   Textile Retail: [www.pronovias.com](http://www.pronovias.com/) : 300,000 Euros

    -   Luxury garments Retail: [www.tous.cn](http://www.tous.cn/) : 400,000 Euros

    -   Food Retail: [www.mercadona.com](http://www.mercadona.com/) : 2-3 mEuros

    -   Luxury Watches Retail: [www.festinagroup.com](http://www.festinagroup.com/) : 1 mEuros

    -   Professional Hairdress: [www.salerm.com](http://www.salerm.com/) : 400,000 Euros

    -   Perfumes Retail: Unidroco, Julia,

    -   Cosmetics/Laboratories: Novartis

-   Google SEO, SEM, Marketing campaigns -- SEO was 1<sup>st</sup> position during years

-   2<sup>nd</sup> in volume sales in Euro area 6mEuros for Megamat brand

-   3 years of growing visitors

**Business Consultant** – [www.kardex.com](http://www.kardex.com/) Feb 2003 – Feb 2006, Barcelona

-   Sales manager in intra-logistic automation -

-   Negotiating and deals

**Engineer Maintenance** – [www.diamed.com](http://www.diamed.com/) - Jun 1998 - Sep 2003, Madrid

-   Repairing, teaching and reporting clinic diagnose electro-medicine machines. My interlocutors were doctors and nurses.

## **EDUCATION**

-   Coderbunker Machine Learning Workshops 2018 (Python, pandas, scikit-learn library 3 workshops)
      Advanced workshops to see the scope of machine learning methodologies
      - Image face Recognition 
      - Recomendation System
      - Multivariable Analysis

-   Coursera:  Machine Learning Standford 2017 (3/4 Months, basic fundations)

-   Fudan University Chinese Language I, Chinese Language, 2015 – 2016

-   Neurolinguistic programming (NLP) : Practicioner - 2013

-   BA (Honor's) Politics and Economics OU University – London – 2010 – 2015

-   Market Research Master, Marketing UOC University – Barcelona - 2006 - 2010

-   Electronics Engineer Bachelor’s UAH University – Madrid - 1993 – 1997
    -   Artificial Vision system follow an Object, made in Borland C 
    -   1<sup>st</sup> obtaining degree within 120 people, I spent 1 year less than the rest of students.


