## 数字项目 

** Linux系统管理员，结合Ansible在VMware和AWS云计算平台上实现运维自动化(DevOps) **

2018年6月至今 上海

-   监控系统维护服务(12小时/5天)

-   为CentOS里面的SNMP Traps实现运维自动化 – 提高可靠性，为基础设施写代码：

**交易数字硬币自动化项目投资**

2018年3月-6月 上海

-   从利益角度说服投资者投资于数字硬币自动化项目

-   谈判价值50万元的种子投资 （最后谈判获胜了吗？）

**翻译网络API （node.js、JS及python）**

2018年1月至3月 上海

-   后端和前端

-   学习和理解中文文档的多种功能，如OCR和自动翻译

**结合Ansible做采矿平台建设的运维自动化(DevOps)**

2017年12月 上海

-   通过Ansible自动化工具部署6个AMD GPU，其预期投资回报率为200%

**市场研究进出口–Jscript**

2017年10月 上海

-   抓取信息公开招标数据库-减少1/1000的时间让关键公司使用金属产品

**结合Ansible为DLG (数字奢侈品代理)做运维自动化(DevOps)**

2017年7月-8月 上海

-   为digitalsociety.com自动部署数字基础设施 避免错误，将服务器的维护时间减少到1/50。

**结合Ansible为eiceducation.com做运维自动化(DevOps)**

2016年12月 上海

-   部署和维护服务器基础设施， 避免错误，将服务器的维护时间减少到1/50。

**数据分析–Jscript**

2017 上海

-   基于财务数据和爬虫下来的约100万条信息，分析哪些是客户，做市场研究分析

-   数据导入和导出, 爬虫 – 市场研究分析

**数据分析–R**

2013/2014 巴塞罗那

用Jupyter做因子分析——客户市场研究

**业务开发和营销经验**

**商务开发商-www.grandtop.cn **

2017年2月-2019年3月 上海

-   策划与中国合作伙伴建立一家西班牙公司

-   领导欧洲和美国的研究事业—发掘大约50个公司/实验室的合作机会

-   做西班牙客户与中国供应商之间的商务调解人

-   在位于马德里的西班牙最大的电气实验室，挖掘一个大约2个Meuros个项目

**商务开发商–www.tornae.com -2018**

现在 上海

-   创造自己的事业。

-   做中国供应商和西班牙买家之间的采购代理，发展国际业务-找到一个客户

**营销经理-www.prittypigments.com**

2015年9月-2017年1月 上海

-   说服投资金融数据库检索关键数据

-   从全球金融数据提供商向全世界的主要客户提供大约1000个关键信息—在很短的时间内提供关键信息。

-   网站建设与维护，SEO分析

**营销经理-www.etsystems.com **

2010年1月–2015年1月 巴塞罗那

通过客户市场调查和说服技巧，我创建了以下项目：

-   纺织品零售：www.zara.com:2-3百万欧元

-   纺织品零售： www.desigual.com 2百万欧元

-   纺织品零售：www.pronovias.com 300000欧元

-   奢侈品零售：www.tous.cn 400000欧元

-   食品零售：www.mercdona.com 2-3 meuros

-   豪华手表零售：www.festinagroup.com 1 meuros

-   专业美发：www.salerm.com 400000欧元

-   香水零售：Unidroco，Julia，

-   化妆品/实验室：诺华

-   谷歌搜索引擎优化，扫描电镜，营销活动——搜索引擎优化在一年内排名第一。

-   Megamat品牌在欧元区的销量排名第二，为600万欧元。

-   3年不断增长的游客

**商业顾问–www.kardex.com **

2003年2月至2006年2月 巴塞罗那

-   物流自动化销售经理

-   谈判和交易

**工程师维护–www.diamed.com **

1998年6月-2003年9月 马德里

维修、教学和报告临床诊断电子医疗机器。我的对话者是医生和护士。

**学习和教育**

## **学历：**

- 伦敦大学政治经济学荣誉学士
- 市场研究硕士，营销UOC大学-巴塞罗那
- 电子工程师，阿联酋大学学士-马德里 （第一次拿到学位的人在120人以内，我花了比其他学生少一年的时间。）

**其他教育和学习经验：**

-   课程：机器学习标准2017 (3/4个月，基本资金)

-   复旦大学汉语I，汉语，2015-2016

-   神经语言程序设计 (NLP)：实习师-2013

-   人造视觉系统跟随一个物体，在Borland C制造

2018年参加coderbunker组织的python、pandas、scikit学习库3机器学习个研讨会，并参与高级研讨会，了解机器学习方法的范围。

-   图像人脸识别

-   推荐系统

-   多变量分析

##**语言：**

西班牙语、英语、加泰罗尼亚语、中文

##**编程语言：**

Bash, Python, Nodejs, Javascript, Html, Css, R

##*协作**

coderbunker.com(上海第一个软件开发社区)的第一个成员。
